[all:vars]
easy_openshift_repo_image="registry.gitlab.com/michaelgugino/easy-openshift/openshift-rpms:v3.11.0"

[OSEv3]

[OSEv3:children]
masters
nodes
etcd
new_nodes

[OSEv3:vars]
ansible_ssh_user=fedora
ansible_python_interpreter=/usr/bin/python3
ansible_become=True
openshift_deployment_type=origin
openshift_release=v3.11
openshift_master_default_subdomain="app.openshift.com"
openshift_storage_glusterfs_storageclass_default=True

[masters]
fedora1.openshift.com

[masters:vars]
openshift_node_group_name="node-config-all-in-one"
glusterfs_devices=['/dev/vdb']
[etcd:children]
masters

[new_nodesz]


[new_nodes]
fedora6.openshift.com

[new_nodes:vars]
openshift_node_group_name="node-config-compute"
glusterfs_devices=['/dev/vdb']

[nodes]

[nodes:vars]


[n2]
fedora2.openshift.com
fedora3.openshift.com

[n2:vars]
openshift_node_group_name="node-config-infra"
glusterfs_devices=['/dev/vdb']

[nodes:children]
masters
n2
new_nodes

[glusterfs]

[glusterfs:children]
nodes

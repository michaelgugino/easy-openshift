#!/bin/bash
set -e
APB3="`which python3` `which ansible-playbook`"
WORKDIR=$PWD
cd ~/git/openshift-ansible
$APB3 -i $WORKDIR/inv-fedora.txt -e @$WORKDIR/extra_vars.yml ~/git/openshift-ansible/playbooks/adhoc/uninstall.yml -vvv

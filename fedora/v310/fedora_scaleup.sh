#!/bin/bash
set -e
APB3="`which python3` `which ansible-playbook`"
WORKDIR=$PWD
cd ~/git/openshift-ansible
$APB3 -i $WORKDIR/inv-fedora.txt -e @$WORKDIR/extra_vars.yml ~/git/easy-openshift/playbooks/prep.yml -vvv -l new_nodes
$APB3 -i $WORKDIR/inv-fedora.txt -e @$WORKDIR/extra_vars.yml ~/git/easy-openshift/playbooks/localrepo.yml -vvv -l new_nodes
# $APB3 -i $WORKDIR/inv-fedora.txt -e @$WORKDIR/extra_vars.yml ~/git/openshift-ansible/playbooks/prerequisites.yml -vvv
$APB3 -i $WORKDIR/inv-fedora.txt -e @$WORKDIR/extra_vars.yml ~/git/openshift-ansible/playbooks/openshift-node/scaleup.yml -vvv

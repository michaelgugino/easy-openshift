[all:vars]
easy_openshift_repo_image="registry.gitlab.com/michaelgugino/easy-openshift/openshift-rpms:v3.9.0"

[OSEv3]

[OSEv3:children]
masters
nodes
etcd

[OSEv3:vars]
ansible_ssh_user=fedora
ansible_python_interpreter=/usr/bin/python3
ansible_become=True
openshift_deployment_type=origin
openshift_release=v3.9
openshift_master_default_subdomain="app.example.com"
[masters]
fedora2.openshift.com openshift_node_labels="{'region': 'infra','zone': 'default'}" openshift_schedulable=true

[etcd:children]
masters

[nodes]

[nodes:children]
masters

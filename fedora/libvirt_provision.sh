#!/bin/bash

# Script to create some instances.  Not really idempotent.
APB3="`which python3` `which ansible-playbook`"
WORKDIR=$PWD
cd $HOME/git/cloudvm_maker
$APB3 -i $WORKDIR/inventory.txt main.yml

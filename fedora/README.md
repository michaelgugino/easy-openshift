How to provision libvirt cloud images.

1. Add to main section of /etc/NetworkManager.conf
```sh
[main]
dns=dnsmasq
```

2. Adjust libvirt network/dns settings

3. Provision instances

4. Restart NetworkManager

5. Add dnsmasq config /etc/NetworkManager/dnsmasq.d/libvirt_dnsmasq.conf

```sh
# Wildcard entry for applications; should point at router host (in our case, any master node).
# should match openshift_master_default_subdomain, except for leading '.'
address=/.app.example.com/192.168.124.63

# libvirt binds a dnsmasq instance on the virtual network; we configured libvirt to respond for this domain,
# ensure we delegate resolving this domain to libvirt.
server=/example.com/192.168.124.1
```

Run libvirt_provision.sh after filling out inventory variables; see group_vars.

#!/bin/bash

qemu-img create -f qcow2 ~/images/fedora27-001/vdb.qcow2 15G
qemu-img create -f qcow2 ~/images/fedora27-002/vdb.qcow2 15G
qemu-img create -f qcow2 ~/images/fedora27-003/vdb.qcow2 15G

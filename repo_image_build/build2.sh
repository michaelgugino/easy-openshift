#!/bin/bash
set -x
PACKAGES="nginx"

# This will install the package to our build host, ensuring we get all the
# necessary dependencies.
# dnf clean all --installroot $CONTAINER_ROOT --releasever 27
dnf install --installroot $CONTAINER_ROOT --releasever 28 $PACKAGES -y

cp $HOME/git/origin/_output/local/releases/rpms $CONTAINER_ROOT/usr/share/nginx/html

buildah $BUILDAH_ARGS config --cmd "/usr/sbin/nginx -g 'daemon off;'" $CONTAINER
buildah $BUILDAH_ARGS config --port 80/tcp $CONTAINER

buildah $BUILDAH_ARGS commit $CONTAINER docker-daemon:nginx-repo:latest

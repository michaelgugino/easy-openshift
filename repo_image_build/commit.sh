#!/bin/bash
# first arg is commit location + tag, eg:
# docker-daemon:nginx-repo:latest
# or docker://registry.gitlab.com/michaelgugino/easy-openshift/openshift-rpms:v3.9.0
buildah $BUILDAH_ARGS commit $CONTAINER $1

#!/bin/bash

# RPM_DIR is the folder containing the RPMs.
RPM_DIR=$1
cp $RPM_DIR $CONTAINER_ROOT -r

# How to build the repo container

You shouldn't use random binaries from the internet, and you shouldn't use
random container images either.

This directory provides some of the tools necessary to create your own
'repo' images.  You must compile the rpms yourself and copy them into
the container.

The container should have a directory at /rpms containing the rpms.

# steps

```
. start
./build.sh path/to/rpms
./commit.sh docker://myreg.com/prj/image:v1
sh clean.sh
```

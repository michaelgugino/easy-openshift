# Steps

## Clone everything to $HOME/git

1. git clone https://github.com/openshift/openshift-ansible.git $HOME/git/openshift-ansible

2. git clone https://gitlab.com/michaelgugino/cloudvm_maker.git $HOME/git/cloudvm_maker

* Note: only required for libvirt provisioning.

## Checkout appropriate versions.

Ensure you checkout the version of openshift-ansible you
wish to deploy.

## Provision VMs
You can provision some instances in the usual way.  If you wish to deploy instances
utilizing libvirt, we have provided some steps to make that easier in fedora/ dir.
